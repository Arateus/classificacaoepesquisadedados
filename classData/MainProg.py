#!/usr/bin/env python3

#import das bibliotecas usadas
import pickle
import json
from trie import Trie
import string
from pprint import pprint
import unicodedata
import re
import copy
import os


# função main
def main():
#varíaveis globais usadas no trabalho
    global descTrie
    global descList
    global nameTrie
    global level
    global db
    db = []

    print("YuGiOh Almighty Database!\n")

#abertura do arquivo binário caso ja tenha sido criado antes
    try:
        with open('cards.bin' , "rb") as cards: #abertura do arquivo binário usando Pickle
            db = pickle.load(cards)
            print ("Banco de Dados carregado!")
            level = db.pop()
            descList = db.pop()
            descTrie = db.pop()
            nameTrie = db.pop()


#caso seja a primeira vez rodando o programa
    except:
        print("Criando Banco de Dados...")
        gerarData()

    menuInterativo()


# função que printa o menu e lê a entrada do usuário
def menuDb():
    escolha = 0
    
    while (int(escolha) == 0 or int(escolha) > 8):
        print("\nEscolha a operação a ser feita:")
        print("\t 1 Procurar por nome")
        print("\t 2 Procurar pelo ID")
        print("\t 3 Procurar pelo ataque")
        print("\t 4 Procurar pela defesa")
        print("\t 5 Procurar pelo level")
        print("\t 6 Procurar por mais de um componente (ataque, defesa, level)")
        print("\t 7 Procurar por uma palavra da descrição")
        print("\t 8 Sair\n")
        escolha = input()

    return escolha

# função que realiza a vontade do usuário
def menuInterativo():
    sair = 0

    while (sair == 0):
        escolha = 0
        escolha = int(menuDb())

        if escolha == 1:
            nome = input("Qual o nome da carta? ")
            nome = normalizeText(nome)
            if nameTrie.__contains__(nome): #ve se a carta ta na database
                carta = procuraNome(nome) #se esta, vai para a funcao de procurar por nome
                printCarta(carta)       #vai para a funcao de impressao da carta na tela
            else:
                print("Nome incorreto ou inexistente. ")

            #print("\n")
            os.system('pause')


        elif escolha == 2:
            ID = eval(input("Qual o ID da carta? "))
            carta = {}
            for i in range(len(db)):
                if(db[i]['id'] == ID):      #procura na db se existe o id igual ao digitado
                    carta = db[i]           #se existe coloca a carta em "carta"
            if (carta):                     #se a carta foi encontrada vai para a funcao de impressao da carta na tela
                printCarta(carta)
            else:
                print("ID incorreto ou inexistente. ")

            #print("\n")
            os.system('pause')

        elif escolha == 3:
            ataque = eval(input("Qual o ataque desejado? "))
            tipoOrd = 0
            while (int(tipoOrd) == 0 or int(tipoOrd) > 3):      #pede se o usuario quer ordenar as cartas de mesmo ataque por level, defesa ou id
                print("\nEscolha a operação a ser feita:")
                print("\t 1 Ordenar pelo level")
                print("\t 2 Ordenar pela defesa")
                print("\t 3 Ordenar pelo ID")
                tipoOrd = eval(input())
            forma = 0
            while (int(forma) == 0 or int(forma) > 2):          #pede se o usuario quer as cartas com ordenacao crescente ou decrescente
                print("\nEscolha a operação a ser feita:")
                print("\t 1 Ordenação crescente")
                print("\t 2 Ordenação decrescente")
                forma = eval(input())
            listaAux = []
            listaAux = procuraAtaque (ataque, tipoOrd, forma, db)              #chama a funcao que procura e ordena as cartas conforme certo ataque
            if (listaAux != ["ERRO"]):                  #testa se existe cartas com esse atributo
                for i in range(0,len(listaAux)):        #for para a impressão de cartas
                    carta = listaAux.pop()
                    printCarta(carta)
            else:
                print("Nao existe cartas com esse atributo.")
            #print("\n")
            os.system('pause')

        elif escolha == 4:
            defesa = eval(input("Qual a defesa desejada? "))
            tipoOrd = 0
            while (int(tipoOrd) == 0 or int(tipoOrd) > 3):      #pede se o usuario quer ordenar as cartas de mesma defesa por level, ataque ou id
                print("\nEscolha a operação a ser feita:")
                print("\t 1 Ordenar pelo level")
                print("\t 2 Ordenar pelo ataque")
                print("\t 3 Ordenar pelo ID")
                tipoOrd = eval(input())
            forma = 0
            while (int(forma) == 0 or int(forma) > 2):          #pede se o usuario quer as cartas com ordenacao crescente ou decrescente
                print("\nEscolha a operação a ser feita:")
                print("\t 1 Ordenação crescente")
                print("\t 2 Ordenação decrescente")
                forma = eval(input())
            listaAux = []
            listaAux = procuraDefesa (defesa, tipoOrd, forma, db)              #chama a funcao que procura e ordena as cartas conforme certa defesa
            if (listaAux != ["ERRO"]):                  #testa se existe cartas com esse atributo
                for i in range(0,len(listaAux)):        #for para a impressão de cartas
                    carta = listaAux.pop()
                    printCarta(carta)
            else:
                print("Nao existe cartas com esse atributo.")
            #print("\n")
            os.system('pause')

        elif escolha == 5:
            nivel = eval(input("Qual o level desejado? "))
            tipoOrd = 0
            while (int(tipoOrd) == 0 or int(tipoOrd) > 3):      #pede se o usuario quer ordenar as cartas de mesmo level por ataque, defesa ou id
                print("\nEscolha a operação a ser feita:")
                print("\t 1 Ordenar pelo ataque")
                print("\t 2 Ordenar pela defesa")
                print("\t 3 Ordenar pelo ID")
                tipoOrd = eval(input())
            forma = 0
            while (int(forma) == 0 or int(forma) > 2):          #pede se o usuario quer as cartas com ordenacao crescente ou decrescente
                print("\nEscolha a operação a ser feita:")
                print("\t 1 Ordenação crescente")
                print("\t 2 Ordenação decrescente")
                forma = eval(input())
            listaAux = []
            listaAux = procuraLevel (nivel, tipoOrd, forma, db)              #chama a funcao que procura e ordena as cartas conforme certo level
            if (listaAux != ["ERRO"]):                  #testa se existe cartas com esse atributo
                for i in range(0,len(listaAux)):        #for para a impressão de cartas
                    carta = listaAux.pop()
                    printCarta(carta)
            else:
                print("Nao existe cartas com esse atributo.")
            #print("\n")
            os.system('pause')


        elif escolha == 6:
            tipoProc = 0
            tipoOrd = 0
            while (int(tipoProc) == 0 or int(tipoProc) > 4):            #pede quais são os atributos que usuário quer procurar
                print("\nEscolha a operação a ser feita:")
                print("\t 1 Procurar pelo level e ataque")
                print("\t 2 Procurar pelo level e defesa")
                print("\t 3 Procurar pelo ataque e defesa")
                print("\t 4 Procurar pelo level, ataque e defesa")
                tipoProc = eval(input())
            if(tipoProc == 1):                                          #atributos: nivel e ataque
                nivel = eval(input("Qual o level desejado? "))
                ataque = eval(input("Qual o ataque desejado? "))
                defesa = 0
                while (int(tipoOrd) != 3 and int(tipoOrd) != 4):        #ordenacao: defesa e id
                    print("\nEscolha a operação a ser feita:")
                    print("\t 3 Ordenar pela defesa")
                    print("\t 4 Ordenar pelo ID")
                    tipoOrd = eval(input())
            if(tipoProc == 2):                                          #atributos: nivel e defesa
                nivel = eval(input("Qual o level desejado? "))
                ataque = 0
                defesa = eval(input("Qual a defesa desejada? "))
                while (int(tipoOrd) != 2 and int(tipoOrd) != 4):        #ordenacao: ataque e id
                    print("\nEscolha a operação a ser feita:")
                    print("\t 2 Ordenar pelo ataque")
                    print("\t 4 Ordenar pelo ID")
                    tipoOrd = eval(input())
            if(tipoProc == 3):                                          #atributos: ataque e defesa
                nivel = 0
                ataque = eval(input("Qual o ataque desejado? "))
                defesa = eval(input("Qual a defesa desejada? "))
                while (int(tipoOrd) != 1 and int(tipoOrd) != 4):        #ordenacao: level e id
                    print("\nEscolha a operação a ser feita:")
                    print("\t 1 Ordenar pelo level")
                    print("\t 4 Ordenar pelo ID")
                    tipoOrd = eval(input())
            if(tipoProc == 4):                                          #atributos: nivel, ataque e defesa Ordenacao: id
                nivel = eval(input("Qual o level desejado? "))
                ataque = eval(input("Qual o ataque desejado? "))
                defesa = eval(input("Qual a defesa desejada? "))
            forma = 0
            while (int(forma) == 0 or int(forma) > 2):          #pede se o usuario quer as cartas com ordenacao crescente ou decrescente
                print("\nEscolha a operação a ser feita:")
                print("\t 1 Ordenação crescente")
                print("\t 2 Ordenação decrescente")
                forma = eval(input())
            listaAux = []
            listaAux = procuraVarios(nivel, ataque, defesa, tipoProc, tipoOrd, forma)   #vai para a funcao que ordena com varios atributos
            if (listaAux != ["ERRO"]):                  #testa se existe cartas com esse atributo
                for i in range(0,len(listaAux)):        #for para a impressão de cartas
                    carta = listaAux.pop()
                    printCarta(carta)
            else:
                print("Nao existe cartas com esse atributo.")
            #print("\n")
            os.system('pause')
            
        elif escolha == 7:
            palavra = input("Qual é a palavra contida na descricao da carta? ") 
            palavra = normalizeText(palavra)                #normaliza a palavra
            if descTrie.__contains__(palavra):              #analisa se essa palavra esta na descTrie
                Cartas = procuraDesc(palavra)               #se existe vai para a funcao que retorna um array com as cartas que contem essa palavra na descricao
                printArrayCartas(Cartas)                    #vai para a funcao que imprime um array de cartas na tela
            else:
                print('Essa palavra nao aparece em nenhuma descricao')

            print("\n")
            os.system('pause')

            
        elif escolha == 8:
            #print("\n")
            print("Fim de Programa")

            sair = 1
        else:
            sair = 0



#Função que pega o arquivo .json e cria o banco de dados
#a ordem dos arquivos na database é:
# 1: [0, 1) -> cartas[]
# 2: [1, 2) -> nameTrie
# 3: [2, 3) -> descTrie
# 4: [3, 4) -> descList
# 5: [4, 5) -> level
def gerarData():
#variaveis globais a serem usadas
    global descTrie
    global descList
    global nameTrie
    global level

#breve definição da Trie e da Lista da descrição das cartas
    descTrie = Trie()
    descList = []
    listaInversaEnd = 0

#abertura do arquivo .json
    with open('cards.json', encoding="utf8") as data:
        jsonDataStream = json.load(data)

    if jsonDataStream:
        print('Arquivo .json carregado')
    else:
        print('ERROR')

    jsonData = {}

#pequeno ajuste no arquivo json
    for di in jsonDataStream:
        jsonData[di['id']] = {}
        for k in di.keys():
            jsonData[di['id']][k]=di[k]

    #print (jsonData)
#criando um IDENTificador para ajudar a manipulação e colocando cada elemento na DB
    i = 0
    for name,card in jsonData.items():
        db.append(jsonData[name])
        db[i]['ident'] = i
        i+=1


#criação da arvore TRIE contendo a descrição das cartas
    print("Carregando proximas etapas...")

    for j in range(0 , len(db)):
        listaAux=[]
        if('desc' in db[j]):
            listaAux=re.sub("[^\w]"," ", db[j]['desc']).split()
        if(len(listaAux) == 0):
            print("ERRO NA CRIAÇÃO DA TRIE %" % j)
        else:
            for k in range(0, len(listaAux)):
                listaAux[k]= listaAux[k].lower()
                if (descTrie.__contains__(listaAux[k])):
                    end = descTrie.__getitem__(listaAux[k])
                    if j not in descList[end]['cards']:
                        descList[end]['cards'].append(j)
                else:
                    descTrie.__setitem__(listaAux[k], listaInversaEnd)
                    auxiliar = {}
                    auxiliar['word'] = listaAux[k]
                    auxiliar['cards'] = []
                    auxiliar['cards'].append(j)
                    descList.append(auxiliar)
                    listaInversaEnd +=1
    print("Trie com a descrição das cartas criada")

#chamando a função que irá indexar o 'level' das cartas
    levelIndex()

#função para criar a árvore TRIE com os nomes
    criarNameTrie()

#concatena tudo o que restou na DB
    db.append(nameTrie)
    db.append(descTrie)
    db.append(descList)
    db.append(level)

#coloca tudo no arquivo binário usando Pickle
    print("Criando arquivo binário")
    with open('cards.bin' , 'wb') as cards:
        pickle.dump(db, cards)

    levelX = db.pop()
    descListX = db.pop()
    descTrieX = db.pop()
    nameTrieX = db.pop()
    print("-------------------------------")

#função que cria a Trie com os nomes das cartas
def criarNameTrie():
    global nameTrie
    nameTrie = Trie()
    for i in range(0, len(db)):
        cardName = normalizeText(db[i]['name'])
        if cardName:
            nameTrie.__setitem__(cardName, i)
    print("Trie com os nomes criado")

#função auxiliar para normalizar String
def normalizeText(textoInicial):
    texto = textoInicial.strip()
    for pc in string.punctuation:
        texto = texto.replace(pc, '')
    texto = texto.lower()
    return texto

#organiza e cria um dicionario com os level's respectivos de cada carta
def levelIndex():
    global level
    level = {}
    level['0'] = []
    level['1'] = []
    level['2'] = []
    level['3'] = []
    level['4'] = []
    level['5'] = []
    level['6'] = []
    level['7'] = []
    level['8'] = []
    level['9'] = []
    level['10'] = []
    level['11'] = []
    level['12'] = []
    level['outro'] = []

    for i in range(0, len(db)):
        if('level' in db[i]):
            if(db[i]['level'] == 0):
                level['0'].append(i)
            elif(db[i]['level'] == 1):
                level['1'].append(i)
            elif (db[i]['level'] == 2):
                level['2'].append(i)
            elif (db[i]['level'] == 3):
                level['3'].append(i)
            elif (db[i]['level'] == 4):
                level['4'].append(i)
            elif (db[i]['level'] == 5):
                level['5'].append(i)
            elif (db[i]['level'] == 6):
                level['6'].append(i)
            elif (db[i]['level'] == 7):
                level['7'].append(i)
            elif (db[i]['level'] == 8):
                level['8'].append(i)
            elif (db[i]['level'] == 9):
                level['9'].append(i)
            elif (db[i]['level'] == 10):
                level['10'].append(i)
            elif (db[i]['level'] == 11):
                level['11'].append(i)
            elif (db[i]['level'] == 12):
                level['12'].append(i)
            else:
                levelArrumado = transforma_para_bin(db[i]['level'], 4)
                db[i]['level'] = levelArrumado

                if (db[i]['level'] == 0):
                    level['0'].append(i)
                elif (db[i]['level'] == 1):
                    level['1'].append(i)
                elif (db[i]['level'] == 2):
                    level['2'].append(i)
                elif (db[i]['level'] == 3):
                    level['3'].append(i)
                elif (db[i]['level'] == 4):
                    level['4'].append(i)
                elif (db[i]['level'] == 5):
                    level['5'].append(i)
                elif (db[i]['level'] == 6):
                    level['6'].append(i)
                elif (db[i]['level'] == 7):
                    level['7'].append(i)
                elif (db[i]['level'] == 8):
                    level['8'].append(i)
                elif (db[i]['level'] == 9):
                    level['9'].append(i)
                elif (db[i]['level'] == 10):
                    level['10'].append(i)
                elif (db[i]['level'] == 11):
                    level['11'].append(i)
                elif (db[i]['level'] == 12):
                    level['12'].append(i)
                else:
                    level['outro'].append(i)

    print("Level indexado corretamente")


#funçoes para pequenas correções dos 'level's de algumas chaves do arquivo .json
def transforma_para_bin(numero, quantos_menos_significativos):
    numero_interno = bin(int(numero))
    resultado = pega_ultimos(numero_interno, quantos_menos_significativos)
    return resultado

def pega_ultimos(str, quantos_menos_significativos):
    lista = []
    resultado = 0
    lista_invertida = []
    if(int(quantos_menos_significativos) < len(str)):
        for i in range((len(str) - int(quantos_menos_significativos)),len(str)):
            lista.append(int(str[i]))
            lista_invertida.append(0)
    j = (len(lista)-1)
    for i in range(0,len(lista)):
        if(i < len(lista)):
            lista_invertida[i] = lista[j]
            j -=1

    for i in range(0,len(lista)):
        resultado = (resultado + (int(lista_invertida[i])*(2**i)))

    return resultado


#-------------------FUNCOES DE BUSCA E ORDENACAO------------------------------
def procuraNome(nome):                      #encontra a carta pelo nome
    carta = nameTrie.__getitem__(nome)      #pega na nameTrie o "indice" dessa carta conforme seu nome
    return db[carta]                        #retorna a carta

def procuraAtaque(ataque, tipoOrd, forma, db):  #encontra as cartas pelo ataque
    listaAux = []
    for i in range(0, len(db)):
        if(db[i]['atk'] == ataque):         #pega as cartas da db que contem o ataque requerido
            listaAux.append(db[i])          #...e as adiciona em uma lista especial
    if not listaAux:
        return ["ERRO"]
    if((tipoOrd == 1) and (forma == 1)):   #ordena essa lista pelo level crescente
        quick_sort(listaAux, 'level')         #chama o quicksort
        listaAux = invertelista(listaAux)   #inverte o resultado
    if((tipoOrd == 1) and (forma == 2)):    #ordena essa lista pelo level decrescente
        quick_sort(listaAux, 'level')         #chama o quicksort
    if((tipoOrd == 2) and (forma == 1)):    #ordena essa lista pela defesa crescente
        quick_sort(listaAux, 'def')         #chama o quicksort
        listaAux = invertelista(listaAux)   #inverte o resultado
    if((tipoOrd == 2) and (forma == 2)):    #ordena essa lista pela defesa decrescente
        quick_sort(listaAux, 'def')         #chama o quicksort
    if((tipoOrd == 3) and (forma == 1)):   #ordena essa lista pelo id crescente
        quick_sort(listaAux, 'id')         #chama o quicksort
        listaAux = invertelista(listaAux)   #inverte o resultado
    if((tipoOrd == 3) and (forma == 2)):    #ordena essa lista pelo id decrescente
        quick_sort(listaAux, 'id')         #chama o quicksort
    return listaAux


def procuraDefesa (defesa, tipoOrd, forma, db): #encontra as cartas pela defesa
    listaAux = []
    for i in range(0, len(db)):
        if(db[i]['def'] == defesa):         #pega as cartas da db que contem a defesa requerida
            listaAux.append(db[i])          #...e as adiciona em uma lista especial
    if not listaAux:
        return ["ERRO"]
    if((tipoOrd == 1) and (forma == 1)):   #ordena essa lista pelo level crescente
        quick_sort(listaAux, 'level')         #chama o quicksort
        listaAux = invertelista(listaAux)   #inverte o resultado
    if((tipoOrd == 1) and (forma == 2)):    #ordena essa lista pelo level decrescente
        quick_sort(listaAux, 'level')         #chama o quicksort
    if((tipoOrd == 2) and (forma == 1)):    #ordena essa lista pelo ataque crescente
        quick_sort(listaAux, 'atk')         #chama o quicksort
        listaAux = invertelista(listaAux)   #inverte o resultado
    if((tipoOrd == 2) and (forma == 2)):    #ordena essa lista pelo ataque decrescente
        quick_sort(listaAux, 'atk')         #chama o quicksort
    if((tipoOrd == 3) and (forma == 1)):   #ordena essa lista pelo id crescente
        quick_sort(listaAux, 'id')         #chama o quicksort
        listaAux = invertelista(listaAux)   #inverte o resultado
    if((tipoOrd == 3) and (forma == 2)):    #ordena essa lista pelo id decrescente
        quick_sort(listaAux, 'id')         #chama o quicksort
    return listaAux


def procuraLevel (nivel, tipoOrd, forma, db):   #encontra as cartas pelo level
    if(nivel < 0 or nivel > 12):
        return ["ERRO"]
    nivel = str(nivel)                      #transforma o nivel solicitado em um string, pois a chave de level é um numero em string
    niveis = copy.deepcopy(level[nivel])    #faz uma cópia da lista de indices das cartas com o level solicitado
    listaAux = []
    while(len(niveis)):
        i = niveis.pop()
        listaAux.append(db[i])              #adiciona em uma lista especial as cartas dos indices obtidos
    if((tipoOrd == 1) and (forma == 1)):   #ordena essa lista pelo ataque crescente
        quick_sort(listaAux, 'atk')         #chama o quicksort
        listaAux = invertelista(listaAux)   #inverte o resultado
    if((tipoOrd == 1) and (forma == 2)):    #ordena essa lista pelo ataque decrescente
        quick_sort(listaAux, 'atk')          #chama o quicksort
    if((tipoOrd == 2) and (forma == 1)):    #ordena essa lista pela defesa crescente
        quick_sort(listaAux, 'def')         #chama o quicksort
        listaAux = invertelista(listaAux)   #inverte o resultado
    if((tipoOrd == 2) and (forma == 2)):    #ordena essa lista pela defesa decrescente
        quick_sort(listaAux, 'def')         #chama o quicksort
    if((tipoOrd == 3) and (forma == 1)):   #ordena essa lista pelo id crescente
        quick_sort(listaAux, 'id')         #chama o quicksort
        listaAux = invertelista(listaAux)   #inverte o resultado
    if((tipoOrd == 3) and (forma == 2)):    #ordena essa lista pelo id decrescente
        quick_sort(listaAux, 'id')         #chama o quicksort
    return listaAux


def procuraVarios(nivel, ataque, defesa, tipoProc, tipoOrd, forma):
    if(tipoProc == 1):                                                  #procura level e ataque
        if(tipoOrd == 3):                                               #ordenação: defesa
            listaAux = procuraLevel(nivel, 2, forma, db)                #chama a funçao de procurar por level
            if (listaAux != ["ERRO"]):                                  #analisa se existem cartas com o atributo anterior
                listaAux = procuraAtaque(ataque, 2, forma, listaAux)    #chama a funçao de procurar por ataque porém com a lista de cartas resultantes da funçao anterior
        if(tipoOrd == 4):                                               #ordenação: id
            listaAux = procuraLevel(nivel, 3, forma, db)                #chama a funçao de procurar por level
            if (listaAux != ["ERRO"]):                                  #analisa se existem cartas com o atributo anterior
                listaAux = procuraAtaque(ataque, 3, forma, listaAux)    #chama a funçao de procurar por ataque porém com a lista de cartas resultantes da funçao anterior
    if(tipoProc == 2):                                                  #procura level e defesa
        if(tipoOrd == 2):                                               #ordenação: ataque
            listaAux = procuraLevel(nivel, 1, forma, db)                #chama a funçao de procurar por level
            if (listaAux != ["ERRO"]):                                  #analisa se existem cartas com o atributo anterior
                listaAux = procuraDefesa(defesa, 2, forma, listaAux)    #chama a funçao de procurar por defesa porém com a lista de cartas resultantes da funçao anterior
        if(tipoOrd == 4):                                               #ordenação: id
            listaAux = procuraLevel(nivel, 3, forma, db)                #chama a funçao de procurar por level
            if (listaAux != ["ERRO"]):                                  #analisa se existem cartas com o atributo anterior
                listaAux = procuraDefesa(defesa, 3, forma, listaAux)    #chama a funçao de procurar por defesa porém com a lista de cartas resultantes da funçao anterior
    if(tipoProc == 3):                                                  #procura ataque e defesa
        if(tipoOrd == 1):                                               #ordenação: level
            listaAux = procuraDefesa(defesa, 1, forma, db)                #chama a funçao de procurar por defesa
            if (listaAux != ["ERRO"]):                                  #analisa se existem cartas com o atributo anterior
                listaAux = procuraAtaque(ataque, 1, forma, listaAux)    #chama a funçao de procurar por ataque porém com a lista de cartas resultantes da funçao anterior
        if(tipoOrd == 4):                                               #ordenação: id
            listaAux = procuraDefesa(defesa, 3, forma, db)                #chama a funçao de procurar por defesa
            if (listaAux != ["ERRO"]):                                  #analisa se existem cartas com o atributo anterior
                listaAux = procuraAtaque(ataque, 3, forma, listaAux)    #chama a funçao de procurar por ataque porém com a lista de cartas resultantes da funçao anterior
    if(tipoProc == 4):                                                  #procura level, ataque e defesa ordenacao: id
        listaAux = procuraLevel(nivel, 3, forma, db)                    #chama a funçao de procurar por level
        if (listaAux != ["ERRO"]):                                  #analisa se existem cartas com o atributo anterior
            listaAux = procuraAtaque(ataque, 3, forma, listaAux)    #chama a funçao de procurar por ataque porém com a lista de cartas resultantes da funçao anterior
        if (listaAux != ["ERRO"]):                                  #analisa se existem cartas com o atributo anterior
            listaAux = procuraDefesa(defesa, 3, forma, listaAux)    #chama a funçao de procurar por defesa porém com a lista de cartas resultantes da funçao anterior
    return listaAux
        
    
    

def procuraDesc(palavra):                   #encontra as cartas por uma palavra na descricao
    index = descTrie.__getitem__(palavra)   #pega os indices das cartas que contem a palavta na descricao
    array = descList[index]                 
    return(array['cards'])                  

def quick_sort(lista, tipo):                 #ordena atraves de quicksort
    if len(lista) > 1:
        pivo = len(lista) / 2                 #define o pivo
        pivo = int(pivo)
        lista_menores = []
        lista_maiores = []
        for i in range(len(lista)):
            valor = lista[i][tipo]
            carta = lista[i]
            if i != pivo:
                if valor < lista[pivo][tipo]:   #analisa se o valor e uma carta é menor que o valor da carta pivo
                    lista_menores.append(carta) #se for a coloca na lista de valores menores
                else:
                    lista_maiores.append(carta) # se nao for a coloca na lista de valores maiores
 
        quick_sort(lista_menores, tipo)         #chama o quicksort para a lista de valores menores
        quick_sort(lista_maiores, tipo)         #chama o quicksort para a lista de valores maiores
        lista[:] = lista_menores + [lista[pivo]] + lista_maiores    #junta os menores com o pivo e os maiores

def invertelista(lista): #inverte a lista com a ajuda de uma lista auxiliar
    aux = []
    for i in range(len(lista)):
        x = lista.pop()
        aux.append(x)
    return aux

#---------------------------------FUNCOES DE IMPRESSAO NA TELA-----------------------
def printCarta(carta):
    if ('name' in carta) == True: 
        print('Nome: {}'.format(carta['name']) )
    if ('id' in carta) == True: 
        print('ID: {}'.format(carta['id']) )
    if ('atk' in carta) == True:  
        print('Ataque: {}'.format(carta['atk']) )
    if ('def' in carta) == True:   
        print('Defesa: {}'.format(carta['def']) )
    if ('level' in carta) == True:        
        print('Level: {}'.format(carta['level']) )
    if ('desc' in carta) == True:    
        print('Descricao: {}'.format(carta['desc']) )
    print("\n")

def printArrayCartas(cartas):
    tamanho = len(cartas)
    if tamanho > 5:
        printcont = 0
        for i in range(0, tamanho):
            if(printcont < 20): #printa 20 de cada vez
                print(db[cartas[i]]['name'] + ' ID:{}'.format(db[cartas[i]]['id']))
                printcont += 1
            else:
                print( 'Ver mais 20? (Sim ou nao)' )
                a = input('>')
                if a.lower() == 'sim':
                    printcont = 0 #printa mais 20
                if a.lower() == 'nao':
                    break
    else:
        for i in range(0,tamanho):
            print(db[cartas[i]]['name'] + ' ID:{}'.format(db[cartas[i]]['id']))

#-----------------------MAIN----------------------------

main()
